# **PRUEBA MAQUETADOR WEB**

### Herramientas utilizadas:
- Webpack
- Gulp
- Sass
- Node JS / npm

### Comandos:
``` 
npm run start
    - Inicia un entorno de desarrollo en el puerto especificado en el archivo de confiracion de webpack
npm run prod
    - Compila el codigo hecho en la carpeta /src para generar un build de produccion
```